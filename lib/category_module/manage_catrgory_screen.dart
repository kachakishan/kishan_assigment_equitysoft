import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kishan_assignment_equitysoft/services/ApiService.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';

class ManageCategoryScreen extends StatefulWidget {
  const ManageCategoryScreen({Key? key}) : super(key: key);

  @override
  State<ManageCategoryScreen> createState() => _ManageCategoryScreenState();
}

class _ManageCategoryScreenState extends State<ManageCategoryScreen> {
  var _categoryname = TextEditingController();
  bool _validate = false;
  final Stream<QuerySnapshot> collectionReference = ApiService.readCategory();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: HexColor("#6D7072"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Category"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextField(
                  controller: _categoryname,
                  autofocus: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.teal)),
                    hintText: 'Category Name',
                    labelText: 'Category Name',
                    errorText: null,
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                TextButton(
                  onPressed: () {
                    setState(() async {
                      print(_categoryname.text.toString());
                      if (_categoryname.text.isEmpty) {
                        _validate == true;
                      } else {
                        _validate == false;
                        final response = await ApiService.addCategory(
                          name: _categoryname.text.toString(),
                        );
                        if (response.code != 200) {
                          _categoryname.text = "";
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          _categoryname.text = "";
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      }
                    });
                  },
                  style: ButtonStyle(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      minimumSize:
                          MaterialStateProperty.all(Size(double.infinity, 44)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0)),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(HexColor("#6D7072"))),
                  child: Text(
                    'Add',
                    style: GoogleFonts.roboto(
                        color: HexColor("#FFFFFF"), fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 30.h,
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    'List of Category',
                    textAlign: TextAlign.start,
                    style: GoogleFonts.roboto(
                      color: HexColor("#6D7072"),
                      fontSize: 14,
                    ),
                  ),
                ),
                Container(
                  height: double.maxFinite,
                  child: StreamBuilder(
                    stream: collectionReference,
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: ListView(
                            children: snapshot.data!.docs.map((e) {
                              return Card(
                                  color: HexColor('#6D7072'),
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: HexColor('#6D7072'), width: 1),
                                    borderRadius: BorderRadius.circular(7),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          e["category_name"].toString(),
                                          style: GoogleFonts.roboto(
                                              color: HexColor("#FFFFFF"),
                                              fontSize: 14),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                          icon: Icon(Icons.delete,
                                              color: Colors.white),
                                          onPressed: () async {
                                            final response =
                                                await ApiService.deleteCategory(
                                                    docId: e.id);

                                            if (response.code != 200) {
                                              _categoryname.text = "";
                                              Fluttertoast.showToast(
                                                  msg: response.message
                                                      .toString(),
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIosWeb: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white,
                                                  fontSize: 16.0);
                                            } else {
                                              _categoryname.text = "";
                                              Fluttertoast.showToast(
                                                  msg: response.message
                                                      .toString(),
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIosWeb: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white,
                                                  fontSize: 16.0);
                                            }
                                          })
                                    ],
                                  ));
                            }).toList(),
                          ),
                        );
                      }

                      return Container();
                    },
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
