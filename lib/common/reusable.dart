import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


Future<dynamic> navigateToScreen(BuildContext context, Widget screen) async {
  var value = await Navigator.push(
      context, CupertinoPageRoute(builder: (context) => screen));
  return value;
}

navigateReplaceToScreen(BuildContext context, Widget screen) {
  Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => screen));
}
