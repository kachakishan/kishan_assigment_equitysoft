import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kishan_assignment_equitysoft/services/ApiService.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';

class ManageCompanyScreen extends StatefulWidget {
  const ManageCompanyScreen({Key? key}) : super(key: key);

  @override
  State<ManageCompanyScreen> createState() => _ManageCompanyScreenState();
}

class _ManageCompanyScreenState extends State<ManageCompanyScreen> {
  final _companyname = TextEditingController();
  bool _validate = false;
  final Stream<QuerySnapshot> collectionCompany = ApiService.readCompany();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: HexColor("#6D7072"),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Company"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                 TextField(
                  controller: _companyname,
                  autofocus: true,
                  decoration:  InputDecoration(
                    border:  OutlineInputBorder(
                        borderSide:  BorderSide(color: Colors.teal)),
                    hintText: 'Company Name',
                    labelText: 'Company Name',
                  ),
                ),
                SizedBox(
                  height: 20.h,
                ),
                TextButton(
                  onPressed: () {
                    setState(() async {
                      print(_companyname.text.toString());
                      if (_companyname.text.isEmpty) {
                        setState(() {
                          _validate == true;
                        });
                      } else {
                        _validate == false;
                        final response = await ApiService.addCompany(
                          name: _companyname.text.toString(),
                        );
                        if (response.code != 200) {
                          _companyname.text = "";
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          _companyname.text = "";
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        }
                      }
                    });
                  },
                  style: ButtonStyle(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      minimumSize:
                          MaterialStateProperty.all(Size(double.infinity, 44)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0)),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(HexColor("#6D7072"))),
                  child: Text(
                    'Add',
                    style: GoogleFonts.roboto(
                        color: HexColor("#FFFFFF"), fontSize: 18),
                  ),
                ),
                SizedBox(
                  height: 30.h,
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    'List of Companies',
                    textAlign: TextAlign.start,
                    style: GoogleFonts.roboto(
                      color: HexColor("#6D7072"),
                      fontSize: 14,
                    ),
                  ),
                ),
                Container(
                  height: double.maxFinite,
                  child: StreamBuilder(
                    stream: collectionCompany,
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (snapshot.hasData) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: ListView(
                            children: snapshot.data!.docs.map((e) {
                              return Card(
                                  color: HexColor('#6D7072'),
                                  shape: RoundedRectangleBorder(
                                    side: BorderSide(
                                        color: HexColor('#6D7072'), width: 1),
                                    borderRadius: BorderRadius.circular(7),
                                  ),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          e["company_name"].toString(),
                                          style: GoogleFonts.roboto(
                                              color: HexColor("#FFFFFF"),
                                              fontSize: 14),
                                        ),
                                      ),
                                      Spacer(),
                                      IconButton(
                                          icon: Icon(Icons.delete,
                                              color: Colors.white),
                                          onPressed: () async {
                                            final response =
                                                await ApiService.deleteCompany(
                                                    docId: e.id);

                                            if (response.code != 200) {
                                              _companyname.text = "";
                                              Fluttertoast.showToast(
                                                  msg: response.message
                                                      .toString(),
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIosWeb: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white,
                                                  fontSize: 16.0);
                                            } else {
                                              _companyname.text = "";
                                              Fluttertoast.showToast(
                                                  msg: response.message
                                                      .toString(),
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIosWeb: 1,
                                                  backgroundColor: Colors.red,
                                                  textColor: Colors.white,
                                                  fontSize: 16.0);
                                            }
                                          })
                                    ],
                                  ));

                              /* return Card(
                                  child: Column(children: [
                                    ListTile(
                                      title: Text(e["category_name"]),
                                      subtitle: Container(
                                        child: (Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text("Position: ",
                                                style: const TextStyle(fontSize: 14)),
                                            Text("Contact Number: " + e.id,
                                                style: const TextStyle(fontSize: 12)),
                                          ],
                                        )),
                                      ),
                                    ),
                                   */ /* ButtonBar(
                                      alignment: MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        TextButton(
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.all(5.0),
                                            primary: const Color.fromARGB(255, 143, 133, 226),
                                            textStyle: const TextStyle(fontSize: 20),
                                          ),
                                          child: const Text('Edit'),
                                          onPressed: () {
                                            Navigator.pushAndRemoveUntil<dynamic>(
                                              context,
                                              MaterialPageRoute<dynamic>(
                                                builder: (BuildContext context) => EditPage(
                                                  employee: Employee(
                                                      uid: e.id,
                                                      employeename: e["employee_name"],
                                                      position: e["position"],
                                                      contactno: e["contact_no"]),
                                                ),
                                              ),
                                                  (route) =>
                                              false, //if you want to disable back feature set to false
                                            );
                                          },
                                        ),
                                        */ /**/ /*TextButton(
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.all(5.0),
                                            primary: const Color.fromARGB(255, 143, 133, 226),
                                            textStyle: const TextStyle(fontSize: 20),
                                          ),
                                          child: const Text('Delete'),
                                          onPressed: () async {
                                            var response =
                                            await FirebaseCrud.deleteEmployee(docId: e.id);
                                            if (response.code != 200) {
                                              showDialog(
                                                  context: context,
                                                  builder: (context) {
                                                    return AlertDialog(
                                                      content:
                                                      Text(response.message.toString()),
                                                    );
                                                  });
                                            }
                                          },
                                        ),*/ /**/ /*
                                      ],
                                    ),*/ /*
                                  ]));*/
                            }).toList(),
                          ),
                        );
                      }

                      return Container();
                    },
                  ),
                ),

                /*Expanded(
                  child: ListView.separated(
                    // controller: _scrollController,
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 10.h,
                        );
                      },
                      shrinkWrap: true,
                      physics: BouncingScrollPhysics(),
                      itemCount: 10,
                      itemBuilder: (BuildContext context, int index) {
                        */ /*   if (index == 0) {
                          return _buildProgressIndicator(context);
                        }*/ /*
                        return listViewForProduct(context, index);
                      }),
                ),*/
              ],
            ),
          ),
        ));
  }

  Widget listViewForProduct(BuildContext context, int index) {
    return Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: HexColor('#0000001A'), width: 1),
          borderRadius: BorderRadius.circular(7),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "LG MONITOT",
                style: GoogleFonts.roboto(
                    color: HexColor("#6D7072"), fontSize: 14),
              ),
            ),
            Spacer(),
            IconButton(
              icon: Icon(Icons.delete, color: Colors.black),
              onPressed: () => {},
            ),
          ],
        ));
  }

  Widget _buildProgressIndicator(BuildContext context) {
    return Text("Hello");
  }
}
