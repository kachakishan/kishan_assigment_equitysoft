import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';
import 'package:kishan_assignment_equitysoft/common/reusable.dart';
import 'package:kishan_assignment_equitysoft/product_module/product_screen.dart';
import 'package:kishan_assignment_equitysoft/category_module/manage_catrgory_screen.dart';
import 'package:kishan_assignment_equitysoft/company_module/manage_company_screen.dart';


class DashboardScreen extends StatefulWidget {
  DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        backgroundColor: const Color(0xff6D7072),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Container(
          color: HexColor("#FFFFFF"),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: () {
                    navigateToScreen(context, ProductScreen());
                  },
                  child: Container(
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: HexColor("#6D7072"),
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Center(
                      child: Text(
                        "Product",
                        style: GoogleFonts.roboto(
                            color: HexColor("#FFFFFF"), fontSize: 22),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (){
                    navigateToScreen(context, ManageCategoryScreen());
                  },
                  child: Container(
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: HexColor("#6D7072"),
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Center(
                      child: Text(
                        "Manage Category",
                        style: GoogleFonts.roboto(
                            color: HexColor("#FFFFFF"), fontSize: 22),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  onTap: (){
                    navigateToScreen(context, ManageCompanyScreen());
                  },
                  child: Container(
                    margin: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: HexColor("#6D7072"),
                      borderRadius: BorderRadius.circular(7),
                    ),
                    child: Center(
                      child: Text(
                        "Manage Company",
                        style: GoogleFonts.roboto(
                            color: HexColor("#FFFFFF"), fontSize: 22),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
