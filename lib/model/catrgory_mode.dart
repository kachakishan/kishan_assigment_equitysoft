import 'package:cloud_firestore/cloud_firestore.dart';

class CategoryModel {
  final String name;
  final String id;


  CategoryModel({
    required this.name,required this.id});

  factory CategoryModel.fromDoc(CategoryDocumentFromFirebase) {
    return CategoryModel(
      id: CategoryDocumentFromFirebase.id,
      name: CategoryDocumentFromFirebase.get("category_name"),
    );
  }
  factory CategoryModel.fromDocument(DocumentSnapshot doc) {
    return CategoryModel(
      id: doc.data().toString().contains('id') ? doc.get('id') : '', //String
      name: doc.data().toString().contains('category_name') ? doc.get('category_name') : '',//Number,//List<dynamic>
    );
  }

}