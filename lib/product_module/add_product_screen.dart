import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kishan_assignment_equitysoft/services/ApiService.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';
import 'package:kishan_assignment_equitysoft/common/reusable.dart';
import 'package:kishan_assignment_equitysoft/dashboardmodule/dashboard_screen.dart';
import 'package:kishan_assignment_equitysoft/product_module/product_screen.dart';

class AddProductScreen extends StatefulWidget {
  final String? product;
  final String? id;
  final String? product_name;
  final String? category_name;
  final String? price;
  final String? qty;
  final String? description;
  final String? company_name;

  const AddProductScreen(
      {super.key,
      this.product,
      this.id,
      this.product_name,
      this.category_name,
      this.price,
      this.qty,
      this.description,
      this.company_name});

  @override
  State<AddProductScreen> createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var product_name = TextEditingController();
  var description = TextEditingController();
  var price = TextEditingController();
  var qty = TextEditingController();

  var companyName, categoryName;
  var setDefaultCompany = true, setDefaultCategory = true;

  @override
  void initState() {
    if (widget.product.toString() == "Edit Product") {
      product_name.text = widget.product_name.toString();
      description.text = widget.description.toString();
      price.text = widget.price.toString();
      qty.text = widget.qty.toString();
      companyName = widget.company_name.toString();
      categoryName = widget.category_name.toString();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: HexColor("#6D7072"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            navigateReplaceToScreen(context, ProductScreen());
          },
        ),
        title: widget.product.toString() == "Edit Product"
            ? const Text("Edit Products")
            : const Text("Add Products"),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.w),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.h,
                ),
                TextFormField(
                    controller: product_name,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Product Name";
                      }
                      return null;
                    },
                    autofocus: true,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('#707070'), width: 1.0),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: HexColor('#707070'))),
                      hintText: 'Product Name',
                      labelText: 'Product Name',
                      hintStyle:
                          TextStyle(color: HexColor("#6D7072"), fontSize: 14),
                      labelStyle:
                          TextStyle(color: HexColor("#6D7072"), fontSize: 18),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                Container(
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: HexColor("#707070"), width: 1)),
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('Category')
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData) return Container();
                      /*if (setDefaultCategory) {
                       // categoryName = snapshot.data?.docs[0].get('category_name');
                       // debugPrint('setDefault make: $categoryName');
                      }*/
                      return DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: categoryName,
                          hint: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Category",
                              style: GoogleFonts.roboto(
                                  color: HexColor("#6D7072"), fontSize: 14),
                            ),
                          ),
                          items: snapshot.data!.docs.map((value) {
                            return DropdownMenuItem(
                              value: value.get('category_name'),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  value.get('category_name'),
                                  style: GoogleFonts.roboto(
                                      color: HexColor("#6D7072"), fontSize: 14),
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              print(value);
                              categoryName = value;
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Container(
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border: Border.all(color: HexColor("#707070"), width: 1)),
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection('Company')
                        .snapshots(),
                    builder: (BuildContext context,
                        AsyncSnapshot<QuerySnapshot> snapshot) {
                      if (!snapshot.hasData) return Container();
                      /* if (setDefaultCompany) {
                       // companyName = snapshot.data!.docs[0].get('company_name');
                       // debugPrint('setDefault make: $companyName');
                      }*/
                      return DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: companyName,
                          hint: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Company",
                              style: GoogleFonts.roboto(
                                  color: HexColor("#6D7072"), fontSize: 14),
                            ),
                          ),
                          items: snapshot.data!.docs.map((value) {
                            return DropdownMenuItem(
                              value: value.get('company_name'),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  value.get('company_name'),
                                  style: GoogleFonts.roboto(
                                      color: HexColor("#6D7072"), fontSize: 14),
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (value) {
                            setState(() {
                              companyName = value;
                            });
                          },
                        ),
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                TextFormField(
                    controller: description,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Description";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    autofocus: true,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('#707070'), width: 1.0),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: HexColor('#707070'))),
                      hintText: 'Description',
                      hintStyle:
                          TextStyle(color: HexColor("#6D7072"), fontSize: 14),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                TextFormField(
                    controller: price,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter Price";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('#707070'), width: 1.0),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: HexColor('#707070'))),
                      hintText: 'Price',
                      hintStyle:
                          TextStyle(color: HexColor("#6D7072"), fontSize: 14),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                TextFormField(
                    controller: qty,
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter qty";
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: HexColor('#707070'), width: 1.0),
                        borderRadius: BorderRadius.circular(4),
                      ),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: HexColor('#707070'))),
                      hintText: 'Qty',
                      hintStyle:
                          TextStyle(color: HexColor("#6D7072"), fontSize: 14),
                    )),
                SizedBox(
                  height: 10.h,
                ),
                Text(
                  'Upload Image',
                  style: GoogleFonts.roboto(
                      color: HexColor("#6D7072"), fontSize: 14),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: 40.h,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 15,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () async {
                            //  await _pickedImage(context, index);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Container(
                              width: 70.h,
                              decoration: const ShapeDecoration(
                                shape: RoundedRectangleBorder(),
                                color: Colors.transparent,
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(1),
                                child: DecoratedBox(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: HexColor('#707070')),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(5.0)),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 30),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      }),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Minimum 2 Image',
                    style: GoogleFonts.roboto(
                        color: HexColor("#6D7072"), fontSize: 14),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                TextButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate() &&
                        companyName.toString().isNotEmpty &&
                        categoryName.toString().isNotEmpty) {
                      if (widget.product.toString() == "Edit Product") {
                        print(widget.product.toString());

                        final response = await ApiService.updateProduct(
                            id: widget.id.toString(),
                            product_name: product_name.text.toString(),
                            category_name: categoryName.toString(),
                            company_name: companyName.toString(),
                            price: price.text.toString(),
                            qty: qty.text.toString(),
                            description: description.text.toString());
                        if (response.code != 200) {
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.yellow,
                              textColor: Colors.white,
                              fontSize: 16.0);
                          navigateReplaceToScreen(context, ProductScreen());
                        }
                      } else {
                        print(widget.product.toString());
                        final response = await ApiService.addProduct(
                            product_name: product_name.text.toString(),
                            category_name: categoryName.toString(),
                            company_name: companyName.toString(),
                            price: price.text.toString(),
                            qty: qty.text.toString(),
                            description: description.text.toString());
                        if (response.code != 200) {
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.red,
                              textColor: Colors.white,
                              fontSize: 16.0);
                        } else {
                          Fluttertoast.showToast(
                              msg: response.message.toString(),
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.yellow,
                              textColor: Colors.white,
                              fontSize: 16.0);
                          navigateReplaceToScreen(context, ProductScreen());
                        }
                      }
                    }
                    _formKey.currentState!.save();
                  },
                  style: ButtonStyle(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      minimumSize:
                          MaterialStateProperty.all(Size(double.infinity, 44)),
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4.0)),
                      ),
                      backgroundColor:
                          MaterialStateProperty.all(HexColor("#6D7072"))),
                  child: Text(
                    'Save',
                    style: GoogleFonts.roboto(
                        color: HexColor("#FFFFFF"), fontSize: 18),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
/*
  Future<void> _pickedImage(BuildContext context, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MyDialog();
        });*/
}

/*
class MyDialog extends StatefulWidget {
  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {


  File? imageFile;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: new SingleChildScrollView(
        child: new ListBody(
          children: <Widget>[
            GestureDetector(
                child: Row(
                  children: <Widget>[
                    Icon(Icons.camera),
                    SizedBox(width: 5),
                    Text('Take a picture'),
                  ],
                ),
                onTap: () async {
                 // await getImageFromCamera();
                  setState(() {

                  });
                }),
            Padding(
              padding: EdgeInsets.all(8.0),
            ),
          ],
        ),
      ),
    );
  }

*/ /*
  Future getImageFromCamera() async {
    PickedFile? pickedFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );
    if (pickedFile != null) {
      setState(() {
         imageFile = File(pickedFile.path as List<Object>);
      });
    }
  }*/ /*

}*/
