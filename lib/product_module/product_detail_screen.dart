import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kishan_assignment_equitysoft/services/ApiService.dart';
import 'package:kishan_assignment_equitysoft/common/reusable.dart';
import 'package:kishan_assignment_equitysoft/product_module/add_product_screen.dart';
import 'package:kishan_assignment_equitysoft/product_module/product_screen.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../common/color_extension.dart';

class ProductDetailScreen extends StatefulWidget {
  final String? id;
  final String? product_name;
  final String? category_name;
  final String? price;
  final String? qty;
  final String? description;
  final String? company_name;

  const ProductDetailScreen(
      {super.key,
      this.id,
      this.product_name,
      this.category_name,
      this.price,
      this.qty,
      this.description,
      this.company_name});

  @override
  State<ProductDetailScreen> createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  CarouselController Controller = CarouselController();

  int activeindex = 0;
  final urlImages = [
    'https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
    'https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: HexColor("#6D7072"),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text("Detail Screen"),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      Center(
                        child: CarouselSlider.builder(
                            itemCount: urlImages.length,
                            itemBuilder: (context, index, realindex) {
                              final urlImage = urlImages[index];
                              return buildImage(urlImage, index);
                            },
                            options: CarouselOptions(
                                height: 160.h,
                                //enlargeCenterPage: true,
                                viewportFraction: 1,
                                //autoplay:true,
                                //pageSnapping:false,
                                //enableInfiniteScroll:false
                                enlargeStrategy:
                                    CenterPageEnlargeStrategy.height,
                                reverse: false,
                                autoPlayInterval: Duration(seconds: 2),
                                onPageChanged: (index, reason) {
                                  setState(() {
                                    activeindex = index;
                                  });
                                })),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 20.h,
                        child: buildIndicator(),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Row(
                    children: [
                      Text(
                        widget.product_name.toString(),
                        style: GoogleFonts.roboto(
                            color: HexColor("#6D7072"),
                            fontSize: 18,
                            fontWeight: FontWeight.normal),
                      ),
                      Spacer(),
                      Text(
                        "Price :${widget.price.toString()}/-",
                        style: GoogleFonts.roboto(
                            color: HexColor("#6D7072"),
                            fontSize: 15,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    widget.category_name.toString(),
                    style: GoogleFonts.roboto(
                        color: HexColor("#6D7072"),
                        fontSize: 14,
                        fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Row(
                    children: [
                      Text(
                        widget.company_name.toString(),
                        style: GoogleFonts.roboto(
                            color: HexColor("#6D7072"),
                            fontSize: 18,
                            fontWeight: FontWeight.normal),
                      ),
                      Spacer(),
                      Text(
                        "Qty:${widget.qty.toString()}",
                        style: GoogleFonts.roboto(
                            color: HexColor("#6D7072"),
                            fontSize: 18,
                            fontWeight: FontWeight.normal),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20.h,
                  ),
                  Text(
                    "Description",
                    style: GoogleFonts.roboto(
                        color: HexColor("#6D7072"),
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                  ),
                  Text(
                    widget.description.toString(),
                    style: GoogleFonts.roboto(
                        color: HexColor("#6D7072"),
                        fontSize: 18,
                        fontWeight: FontWeight.normal),
                  ),
                ],
              ),
            ),
            Expanded(
                child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          width: 100.w,
                          child: ElevatedButton(
                            onPressed: () {
                              navigateToScreen(
                                  context,
                                  AddProductScreen(
                                    product:"Edit Product",
                                    id: widget.id.toString(),
                                    product_name:widget.product_name.toString(),
                                    category_name:widget.category_name.toString(),
                                    price:widget.price.toString(),
                                    qty:widget.qty.toString(),
                                    description:widget.description.toString(),
                                    company_name:widget.company_name.toString(),
                                  ));

                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Edit',
                                style: GoogleFonts.roboto(
                                    color: HexColor("#FFFFFF"), fontSize: 14),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: HexColor('#6D7072'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                // <-- Radius
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 100.w,
                          child: ElevatedButton(
                            onPressed: () {
                              showAlertDialog(context, widget.id.toString());
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Delete',
                                style: GoogleFonts.roboto(
                                    color: HexColor("#FFFFFF"), fontSize: 14),
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor: HexColor('#6D7072'),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                // <-- Radius
                              ),
                            ),
                          ),
                        ),
                      ],
                    )))
          ],
        ),
      ),
    );
  }

  Widget buildIndicator() => Center(
        child: AnimatedSmoothIndicator(
            activeIndex: activeindex,
            count: urlImages.length,
            effect: JumpingDotEffect(
                dotWidth: 10,
                dotHeight: 10,
                activeDotColor: Colors.red,
                dotColor: Colors.black)),
      );

  Widget buildImage(urlImage, int index) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        image:
            DecorationImage(fit: BoxFit.cover, image: NetworkImage(urlImage)),
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Colors.redAccent,
      ),
    );

    /*  Container(
      color: Colors.grey,
      width: double.infinity,
      child: Image.network(
        urlImage,
        fit: BoxFit.cover,
      ),
    );*/
  }

  showAlertDialog(BuildContext context, String id) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        final response = await ApiService.deleteProduct(docId: id);

        if (response.code != 200) {
          Fluttertoast.showToast(
              msg: response.message.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          navigateToScreen(context, ProductScreen());

        } else {
          Fluttertoast.showToast(
              msg: response.message.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          navigateToScreen(context, ProductScreen());
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Are you sure Delete this Product?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
