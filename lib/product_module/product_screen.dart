import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:kishan_assignment_equitysoft/services/ApiService.dart';
import 'package:kishan_assignment_equitysoft/common/color_extension.dart';
import 'package:kishan_assignment_equitysoft/common/reusable.dart';
import 'package:kishan_assignment_equitysoft/product_module/add_product_screen.dart';
import 'package:kishan_assignment_equitysoft/product_module/product_detail_screen.dart';

class ProductScreen extends StatefulWidget {
  const ProductScreen({Key? key}) : super(key: key);

  @override
  State<ProductScreen> createState() => _ProductScreenState();
}

class _ProductScreenState extends State<ProductScreen> {
  final Stream<QuerySnapshot> collectionReference = ApiService.getProductList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: HexColor("#6D7072"),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Products"),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              navigateToScreen(context, AddProductScreen());
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder(
                stream: collectionReference,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasData) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: ListView(
                        children: snapshot.data!.docs.map((e) {
                          return GestureDetector(
                            onTap: () {

                              navigateToScreen(
                                  context,
                                  ProductDetailScreen(
                                    id: e.id,
                                    product_name:e["product_name"].toString(),
                                    category_name:e["category_name"].toString(),
                                    price:e["price"].toString(),
                                    qty:e["qty"].toString(),
                                    description:e["description"].toString(),
                                    company_name:e["company_name"].toString(),
                                  ));
                            },
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: HexColor('#0000001A'), width: 1),
                                  borderRadius: BorderRadius.circular(7),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Expanded(
                                        flex: 2,
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(
                                              5), // Image border
                                          child: SizedBox.fromSize(
                                            size: Size.fromRadius(48),
                                            // Image radius
                                            child: Image.network(
                                                'https://images.pexels.com/photos/462118/pexels-photo-462118.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                                                fit: BoxFit.fill),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.stretch,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: [
                                              Text(
                                                e["product_name"].toString(),
                                                style: GoogleFonts.roboto(
                                                    color: HexColor("#6D7072"),
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.normal),
                                              ),
                                              SizedBox(
                                                height: 5.h,
                                              ),
                                              Text(
                                                e["category_name"].toString(),
                                                style: GoogleFonts.roboto(
                                                    color: HexColor("#6D7072"),
                                                    fontSize: 12,
                                                    fontWeight:
                                                        FontWeight.normal),
                                              ),
                                              SizedBox(
                                                height: 5.h,
                                              ),
                                              Text(
                                                e["qty"].toString(),
                                                style: GoogleFonts.roboto(
                                                    color: HexColor("#6D7072"),
                                                    fontSize: 15,
                                                    fontWeight:
                                                        FontWeight.normal),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 2,
                                        child: Column(
                                          children: [
                                            ElevatedButton(
                                              onPressed: () {
                                                navigateToScreen(
                                                    context,
                                                    AddProductScreen(
                                                      product:"Edit Product",
                                                      id: e.id.toString(),
                                                      product_name:e["product_name"].toString(),
                                                      category_name:e["category_name"].toString(),
                                                      price:e["price"].toString(),
                                                      qty:e["qty"].toString(),
                                                      description:e["description"].toString(),
                                                      company_name:e["company_name"].toString(),
                                                    ));

                                              },
                                              child: Text(
                                                'Edit',
                                                style: GoogleFonts.roboto(
                                                    color: HexColor("#FFFFFF"),
                                                    fontSize: 14),
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                backgroundColor:
                                                    HexColor('#6D7072'),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(5),
                                                  // <-- Radius
                                                ),
                                              ),
                                            ),
                                            ElevatedButton(
                                              onPressed: () async {
                                                showAlertDialog(context, e.id);
                                              },
                                              child: Text(
                                                'Delete',
                                                style: GoogleFonts.roboto(
                                                    color: HexColor("#FFFFFF"),
                                                    fontSize: 14),
                                              ),
                                              style: ElevatedButton.styleFrom(
                                                backgroundColor:
                                                    HexColor('#6D7072'),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          5), // <-- Radius
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                          );
                        }).toList(),
                      ),
                    );
                  }

                  return Container();
                },
              ),
            )
          ],
        ),
      ),
    );
  }




  showAlertDialog(BuildContext context, String id) {
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        final response = await ApiService.deleteProduct(docId: id);

        if (response.code != 200) {
          Fluttertoast.showToast(
              msg: response.message.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          navigateToScreen(context, ProductScreen());
        } else {
          Fluttertoast.showToast(
              msg: response.message.toString(),
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.red,
              textColor: Colors.white,
              fontSize: 16.0);
          navigateToScreen(context, ProductScreen());
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("AlertDialog"),
      content: Text("Are you sure Delete this Product?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
