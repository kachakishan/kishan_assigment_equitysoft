import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:kishan_assignment_equitysoft/model/Response.dart';
import 'package:kishan_assignment_equitysoft/model/catrgory_mode.dart';

class ApiService {
  List<CategoryModel> categorymodel = [];



  //Category Operation
  static Future<Response> addCategory({
    required String name,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = FirebaseFirestore.instance.collection('Category').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "category_name": name,
    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }


  static Stream<QuerySnapshot<Object?>> readCategory() {
    CollectionReference notesItemCollection =
        FirebaseFirestore.instance.collection('Category');

    return notesItemCollection.snapshots();
  }

  static Future<Response> deleteCategory({
    required String docId,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer =
        FirebaseFirestore.instance.collection('Category').doc(docId);

    await documentReferencer.delete().whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully Deleted Category";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }



  //Company Operation

  static Future<Response> addCompany({
    required String name,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer = FirebaseFirestore.instance.collection('Company').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "company_name": name,
    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }


  static Stream<QuerySnapshot<Object?>> readCompany() {
    CollectionReference CompanyItemCollection = FirebaseFirestore.instance.collection('Company');

    return CompanyItemCollection.snapshots();
  }
  static Future<Response> deleteCompany({
    required String docId,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer =
    FirebaseFirestore.instance.collection('Company').doc(docId);

    await documentReferencer.delete().whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully Deleted Company";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

// Product Operation

  static Future<Response> addProduct({
    required String product_name,
    required String category_name,
    required String company_name,
    required String description,
    required String price,
    required String qty,

  }) async {
    Response response = Response();
    DocumentReference documentReferencer = FirebaseFirestore.instance.collection('Product').doc();

    Map<String, dynamic> data = <String, dynamic>{
      "product_name": product_name,
      "category_name":category_name,
      "company_name":company_name,
      "description":description,
      'price':price,
      'qty':qty


    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully added to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }


  static Future<Response> updateProduct({
    required String id,
    required String product_name,
    required String category_name,
    required String company_name,
    required String description,
    required String price,
    required String qty,

  }) async {
    Response response = Response();
    DocumentReference documentReferencer = FirebaseFirestore.instance.collection('Product').doc(id);

    Map<String, dynamic> data = <String, dynamic>{
      "product_name": product_name,
      "category_name":category_name,
      "company_name":company_name,
      "description":description,
      'price':price,
      'qty':qty


    };

    var result = await documentReferencer.set(data).whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully Update to the database";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

  static Stream<QuerySnapshot<Object?>> getProductList() {
    CollectionReference ProductItemCollection = FirebaseFirestore.instance.collection('Product');

    return ProductItemCollection.snapshots();
  }

  static Future<Response> deleteProduct({
    required String docId,
  }) async {
    Response response = Response();
    DocumentReference documentReferencer =
    FirebaseFirestore.instance.collection('Product').doc(docId);

    await documentReferencer.delete().whenComplete(() {
      response.code = 200;
      response.message = "Sucessfully Deleted Product";
    }).catchError((e) {
      response.code = 500;
      response.message = e;
    });

    return response;
  }

}
